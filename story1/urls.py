from django.urls import path
from . import views

app_name = 'story1'

urlpatterns = [
    path('story1/', views.index, name='index'),

]