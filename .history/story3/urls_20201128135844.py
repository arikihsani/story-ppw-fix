from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.index, name='index'),
    path('Contact/', views.Contact, name='Contact'),
    path('tambahmatkul/', views.add_matkul, name='tambahmatkul'),
    path('daftarmatkul/', views.daftar_matkul, name='daftarmatkul'),
    path('delete/<str:pk>', views.delete, name='delete'),
    path('ikutkegiatan/', views.ikut_kegiatan, name='ikutkegiatan'),
    path('daftarkegiatan/', views.daftar_kegiatan, name='daftarkegiatan'),
    path('tambahkegiatan/', views.tambah_kegiatan, name='tambahkegiatan'),
    path('deletekegiatan/<str:pk>', views.deletekegiatan, name='deletekegiatan'),
    path('About/', views.about, name='about'),
]