from django.test import TestCase, Client
from .models import Matkul, Kegiatan, Peserta

# Create your tests here.
class TestWeb(TestCase):
    def test_url_formulir(self):
        response = Client().get('/ikutkegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_url_daftarKegiatan(self):
        response = Client().get('/daftarkegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_url_tambahkegiatan(self):
        response = Client().get('/tambahkegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_models_Kegiatan(self):
        var = Kegiatan.objects.create(nama="nama")