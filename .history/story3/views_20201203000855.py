from django.shortcuts import render
from .forms import FormMatkul, FormKegiatan, FormPeserta
from .models import Matkul, Kegiatan, Peserta
from django.http import HttpResponse
from django.http import JsonResponse
import json
import requests

# Create your views here.

def index(request):
    return render(request, 'profile.html')

def Contact(request):
    return render(request, 'Contact.html')

def add_matkul(request):
    daftar = Matkul.objects.all()
    if request.method=="POST":
        form = FormMatkul(request.POST)
        if form.is_valid():
            simpan = form.save()
            simpan.save()
            form = FormMatkul()

    else:
        form = FormMatkul()

    
    context = {'form':form, 'daftar':daftar}

    return render (request, 'tambahmatkul.html', context)

def ikut_kegiatan(request):
    if request.method=="POST":
        form = FormPeserta(request.POST)
        if form.is_valid():
            simpan = form.save()
            simpan.save()
            form = FormPeserta()

    else:
        form = FormPeserta()

    context = {'form':form}

    return render(request, 'formulirkegiatan.html', context)

def tambah_kegiatan(request):
    daftar = Kegiatan.objects.all()
    if request.method=="POST":
        form = FormKegiatan(request.POST)
        if form.is_valid():
            simpan = form.save()
            simpan.save()
            form = FormKegiatan()

    else:
        form = FormKegiatan()

    context = {'form':form, 'daftar':daftar}

    return render(request, 'tambahkegiatan.html', context)

def daftar_matkul(request):
    daftar = Matkul.objects.all()
    return render(request, 'daftarmatkul.html', {'daftar':daftar})

def daftar_kegiatan(request):
    daftar = Peserta.objects.all()
    context = {'daftar':daftar}
    return render(request, 'lihatkegiatan.html', context)

def delete(request, pk):
    matkul = Matkul.objects.get(id=pk)
    context = {'matkul':matkul}
    if request.method=="POST":
        matkul.delete()
        return render(request, 'sudahhapus.html', context)
    
    return render(request, 'delete.html', context)

def deletekegiatan(request, pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    context = {'kegiatan':kegiatan}
    if request.method=="POST":
        kegiatan.delete()
        return render(request, 'sudahhapuskegiatan.html', context)
    
    return render(request, 'deletekegiatan.html', context)

def about(request):
    return render(request, "About.html")

def story8(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + requests.GET['q']
    ret = requests.get(url)
    hasil = json.loads(ret.content)
    return render(request, "Story8.html")
    return JsonResponse(data, safe=False)



