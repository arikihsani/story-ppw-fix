/* javascript web arik */

$(document).ready(function(){
    $( "#accordion" ).accordion();
    function moveUp(element) {
        if(element.previousElementSibling)
          element.parentNode.insertBefore(element, element.previousElementSibling);
      }
      function moveDown(element) {
        if(element.nextElementSibling)
          element.parentNode.insertBefore(element.nextElementSibling, element);
      }
      document.querySelector('#accordion').addEventListener('click', function(e) {
        if(e.target.className === 'down') moveDown(e.target.parentNode);
        else if(e.target.className === 'up') moveUp(e.target.parentNode);
      });


});

$( "#keyword" ).keyup( function(){
  var input = $("#keyword").val();
  console.log(input);

  $.ajax({
    url: 'https://www.googleapis.com/books/v1/volumes?q=' + input,
    success: function(data){
      console.log(data)
      var array_items = data.items;
      $("#daftar_buku").empty();
      for(i=0; i<array_items.length;i++){
        var judulBuku = array_items[i].volumeInfo.title
        var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
        $("#daftar_buku").append("<li>"+ judulBuku + "<br><img src="+ gambar+"></li>")
        console.log(judulBuku)
      }
    }
  });
});