/* javascript web arik */

$(document).ready(function(){
    $( "#accordion" ).accordion();
    function moveUp(element) {
        if(element.previousElementSibling)
          element.parentNode.insertBefore(element, element.previousElementSibling);
      }
      function moveDown(element) {
        if(element.nextElementSibling)
          element.parentNode.insertBefore(element.nextElementSibling, element);
      }
      document.querySelector('#accordion').addEventListener('click', function(e) {
        if(e.target.className === 'down') moveDown(e.target.parentNode);
        else if(e.target.className === 'up') moveUp(e.target.parentNode);
      });
});

