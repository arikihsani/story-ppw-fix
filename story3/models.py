from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length=100)
    tahun = models.CharField(max_length=10)
    ruangan = models.CharField(max_length=100)

    def __str__(self):
        return self.nama

class Kegiatan(models.Model):
    nama = models.CharField(max_length=50, null=True)
    
    def __str__(self):
        return self.nama


class Peserta(models.Model):
    nama = models.CharField(max_length=50, null=True)
    kegiatan = models.ForeignKey(Kegiatan, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama

    

