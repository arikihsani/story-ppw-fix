from django import forms

from .models import Matkul, Kegiatan, Peserta

class FormMatkul(forms.ModelForm):

    class Meta:
        model = Matkul
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'tahun', 'ruangan']

class FormPeserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama', 'kegiatan']

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama']