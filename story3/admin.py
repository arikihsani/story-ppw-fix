from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Matkul)
admin.site.register(Kegiatan)
admin.site.register(Peserta)